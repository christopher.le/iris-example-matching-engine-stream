from google.cloud import aiplatform_v1beta1
from langchain.embeddings.tensorflow_hub import TensorflowHubEmbeddings
from langchain.embeddings.base import Embeddings
import google.auth

PROJECT_ID = "acn-agbg-ai"
REGION = "us-central1"
INDEX_ID = "chrisle-matching-engine"
ENDPOINT_INDEX_ID = "chrisle_matchin_engine_public"


def get_endpoints_by_endpoint_display_name(endpoint_display_name: str):
    endpoint_url = f"{REGION}-aiplatform.googleapis.com"
    index_endpoint_client = aiplatform_v1beta1.IndexEndpointServiceClient(
        client_options=dict(api_endpoint=endpoint_url)
    )
    request = aiplatform_v1beta1.ListIndexEndpointsRequest(
        parent=f"projects/{PROJECT_ID}/locations/{REGION}",
        filter=f"displayName={endpoint_display_name}"
    )
    response = index_endpoint_client.list_index_endpoints(request=request)

    if not len(response.index_endpoints):
        raise Exception(f"Could not find {endpoint_display_name}?")

    api_endpoint = response.index_endpoints[0].public_endpoint_domain_name
    index_endpoint = response.index_endpoints[0].name
    return api_endpoint, index_endpoint


def find_resource_names_by_index_display_name(
        embedding: Embeddings,
        endpoint_display_name: str,
        query: str):

    credentials, _ = google.auth.default()

    print(f"Get embedding for query")
    embedding = TensorflowHubEmbeddings()
    feature_vector = embedding.embed_query(query)

    print(f"Find nearest neighbors")
    api_endpoint, index_endpoint = get_endpoints_by_endpoint_display_name(endpoint_display_name)

    match_service_client = aiplatform_v1beta1.MatchServiceClient(
        credentials=credentials,
        client_options=dict(api_endpoint=api_endpoint)
    )
    request = aiplatform_v1beta1.FindNeighborsRequest(
        index_endpoint=index_endpoint,
        deployed_index_id=ENDPOINT_INDEX_ID
    )

    dp1 = aiplatform_v1beta1.IndexDatapoint(
        datapoint_id="0",
        feature_vector=feature_vector
    )
    ann_query = aiplatform_v1beta1.FindNeighborsRequest.Query(datapoint=dp1)
    request.queries.append(ann_query)
    response = match_service_client.find_neighbors(request)

    nearest_neighbors = response.nearest_neighbors[0].neighbors
    output = []
    for neighbor in nearest_neighbors:
        distance = neighbor.distance
        datapoint_id = neighbor.datapoint.datapoint_id
        output.append({ "distance": distance, "datapoint_id": datapoint_id })

    return output

# Change this to whatever model you used to create the embeddings
embedding = TensorflowHubEmbeddings()

query = "Mirror, mirror on the wall. Tell me which AI model is the best, overall?"
results = find_resource_names_by_index_display_name(embedding, INDEX_ID, query)

print(f"{results}")
