# GCP Project ID
export PROJECT_ID=acn-agbg-ai

# Region
# Look up valid locations that support Matching Engine here:
# https://cloud.google.com/vertex-ai/docs/general/locations
export REGION=us-central1

# GCP Bucket
# WARNING: Everything in here will be deleted during setup!
export FILE_DROP_BUCKET_NAME=chrisle-matching-engine-input

# Number of dimensions
# Note: Tensorflow Universal Sentence Encoder recommends 512.
#       If you use a different model you may need to change this.
export DIMENSIONS=512

# The Matching Engine display name
export DISPLAY_NAME=chrisle-matching-engine

# Name of the public deployed index.
# This must start with a latter and only include numbers, letters, and underscores.
export DEPLOYED_INDEX_ID=chrisle_matchin_engine_public

# Don't change this.
export ENDPOINT=https://${REGION}-aiplatform.googleapis.com
