# Matching Engine in Stremaing mode example

This folder includes two bash scripts and two Python scripts.

- `1_index_setup.sh`: Creates an index
- `upsert.py`: Upsert a langchain document to the index.
- `2_create_endpoint.sh`: Takes the index and creates an endpoint and makes it public.
- `query.py`: Query the public index.


To delete the index and the index endpoint run `undo_setup.sh`.
