#!/bin/bash
set -e

# Remove the index and the endpoint.

source ./settings.sh

# Delete the end point index.

INDEX_ENDPOINT_ID=$(\
  gcloud ai index-endpoints list --region=${REGION} --filter="displayName=${DISPLAY_NAME}" \
  | grep -o -E "indexEndpoints/([0-9]+)" \
  | cut -d"/" -f2)

INDEX_ID=$(\
  gcloud ai indexes list --region=${REGION} --filter="displayName=${DISPLAY_NAME}" \
  | grep -o -E "indexes/([0-9]+)" \
  | cut -d"/" -f2)

echo "Undeploying index endpoint ${INDEX_ENDPOINT_ID} ${INDEX_ID}..."
gcloud ai index-endpoints undeploy-index ${INDEX_ENDPOINT_ID} \
  --region=${REGION} \
  --deployed-index-id=${DEPLOYED_INDEX_ID}

echo "Deleting index endpoint ${INDEX_ENDPOINT_ID} ..."
gcloud ai index-endpoints delete --region=${REGION} ${INDEX_ENDPOINT_ID}

echo "Deleting index ${INDEX_ID} ..."
gcloud ai indexes delete --region=${REGION} ${INDEX_ID}
