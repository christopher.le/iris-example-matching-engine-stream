#!/bin/bash

source ./settings.sh

# Create an index endpoint
echo "Creating an index endpoint ..."
curl -X POST -H "Content-Type: application/json" \
  -H "Authorization: Bearer `gcloud auth print-access-token`" \
  ${ENDPOINT}/v1/projects/${PROJECT_ID}/locations/${REGION}/indexEndpoints \
  -d '{
    "display_name": "'${DISPLAY_NAME}'",
    "publicEndpointEnabled": "true"
  }'

# Get the index endpoint ID we just created
INDEX_ENDPOINT_ID=$(\
  gcloud ai index-endpoints list --region=${REGION} --filter="displayName=${DISPLAY_NAME}" \
  | grep -o -E "indexEndpoints/([0-9]+)" \
  | cut -d"/" -f2)

# Get the streaming index id from step 1.
INDEX_ID=$(\
  gcloud ai indexes list --region=${REGION} --filter="displayName=${DISPLAY_NAME}" \
  | grep -o -E "indexes/([0-9]+)" \
  | cut -d"/" -f2)

# Deploy the public endpoint
echo "Deploying ${DEPLOYED_INDEX_ID} using index ID (${INDEX_ID}) and endpoint ID (${INDEX_ENDPOINT_ID}) ..."
gcloud ai index-endpoints deploy-index ${INDEX_ENDPOINT_ID} \
  --deployed-index-id=${DEPLOYED_INDEX_ID} \
  --display-name=${DISPLAY_NAME} \
  --index=${INDEX_ID} \
  --project=${PROJECT_ID} \
  --region=${REGION}

echo
echo "*******************************************"
echo "Your Matching Engine index is being created in the background."
echo "(!) This takes about 30-40 minutes (!)"
echo "You can see progress here:"
echo "https://console.cloud.google.com/vertex-ai/locations/us-central1/index-endpoints/${INDEX_ENDPOINT_ID}?project=${PROJECT_ID}"
echo "*******************************************"
echo
