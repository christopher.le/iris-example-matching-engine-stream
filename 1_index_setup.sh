#!/bin/bash

source ./settings.sh

# echo "Deleting all files in the bucket."
# gsutil rm -raf "gs://${BUCKET_NAME}/**" > /dev/null || true

# Create a starter dummy embedding file with n dimensions filled with 0s.
echo "Creating a dummy embedding file with ${DIMENSIONS} dimensions."
value=$((${DIMENSIONS} - 1))
embedding=$(printf '0%.0s,' $(seq 1 $value))
json="{\"id\": \"0\", \"embedding\": [${embedding}0]}"
echo "$json" > data.json

# Copy starter index to the GCP bucket.
echo "Copying dummy embedding file to bucket"
gsutil cp data.json gs://${FILE_DROP_BUCKET_NAME}/data.json

# Delete the starter dummy index. Don't need it anymore.
rm data.json

# Create a streaming index
echo "Creating streaming index"
curl -X POST -H "Content-Type: application/json" \
  -H "Authorization: Bearer `gcloud auth print-access-token`" \
  ${ENDPOINT}/v1/projects/${PROJECT_ID}/locations/${REGION}/indexes \
  -d '{
      displayName: "'${DISPLAY_NAME}'",
      description: "'${DISPLAY_NAME}'",
      metadata: {
        contentsDeltaUri: "'${BUCKET_NAME}'",
        config: {
            dimensions: "'${DIMENSIONS}'",
            approximateNeighborsCount: 150,
            distanceMeasureType: "DOT_PRODUCT_DISTANCE",
            algorithmConfig: {
              treeAhConfig: {
                leafNodeEmbeddingCount: 500,
                leafNodesToSearchPercent: 7
              }
            }
        },
      },
      indexUpdateMethod: "STREAM_UPDATE"
  }'

echo
echo "*******************************************"
echo "Your Matching Engine index is being created in the background."
echo "(!) This takes at least 60 minutes (!)"
echo "You can see progress here:"
echo "https://console.cloud.google.com/vertex-ai/matching-engine/indexes?project=${PROJECT_ID}"
echo "*******************************************"
echo
