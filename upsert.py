"""Example of upserting to a streaming index.

NOTE: I ripped this code out from a class I wrote for another project. This
      code is NOT guaranteed to work out the of box. :)

"""

from google.cloud import aiplatform_v1beta1
from tqdm import tqdm
import time
import uuid

REGION = "us-central1"
ENDPOINT_URL = f"{REGION}-aiplatform.googleapis.com"
PROJECT_ID = "acn-agbg-ai"
INDEX_DISPLAY_NAME = "chrisle-matching-index"


def find_resource_names_by_index_display_name(display_name: str):
    """Get the index name and index_endpoint given a display name.

    Args:
        display_name: Index display name. eg: "my-index"

    Returns:
        (index_name, index_endpoint):
        Where "my-index" would get the full resource name of the index
        and the full resource name of the deployed index.
        eg: ("projects/230707050797/locations/us-central1/indexes/5378661349498814464",
                "projects/230707050797/locations/us-central1/indexEndpoints/3430854510661074944")
    """
    request = aiplatform_v1beta1.ListIndexesRequest(
        parent=f"projects/{PROJECT_ID}/locations/{REGION}",
        filter=f"displayName={INDEX_DISPLAY_NAME}"
    )
    response = index_service_client.list_indexes(request=request)
    index_name = response.indexes[0].name
    index_endpoint = '' #response.indexes[0].deployed_indexes[0].index_endpoint

    if index_name is None:
        raise Exception(f"The index {display_name} doesn't exist. You must exist before you can update it.")

    return index_name, index_endpoint

def upsert_data_points(index_resource_name, embeddings_data):
    """Upserts 1000 data points at a time to the matching engine.

    Args:
        index_resource_name: Index resource name
        embeddings_data:

    """
    # TODO: Implement restricts and crowding_tag.
    datapoints = [
        {"datapoint_id": data["id"], "feature_vector": data["embedding"]}
        for data in embeddings_data
    ]
    print(f"Upserting {len(datapoints)} datapoints to Matching Engine ...")

    # Calculate the number of chunks and chunk size
    chunk_size = 1000
    total_elements = len(datapoints)
    num_chunks = (total_elements + chunk_size - 1) // chunk_size

    with tqdm(total=total_elements, desc="Upsert") as pbar:

        # Upsert one chunk. Retries up to 10 times.
        def upsert_chunk(chunk):
            request = aiplatform_v1beta1.UpsertDatapointsRequest(
                index=index_resource_name,
                datapoints=chunk
            )
            tries = 10
            while tries > 0:
                try:
                    response = index_service_client.upsert_datapoints(request)
                    break  # Successful upsert, exit the loop
                except Exception as e:
                    tries -= 1
                    print(f"Error: {e}. Waiting 15 seconds before retrying (tries left: {tries})")
                    time.sleep(15)  # Delay before retrying

            pbar.update(len(chunk))

        for i in range(num_chunks):
            # Calculate the start and end indices for the current chunk
            start_idx = i * chunk_size
            end_idx = (i + 1) * chunk_size

            # Get the current chunk of data
            chunk = datapoints[start_idx:end_idx]

            upsert_chunk(chunk)


####################################################

from langchain.embeddings.tensorflow_hub import TensorflowHubEmbeddings

index_service_client = aiplatform_v1beta1.IndexServiceClient(
    client_options=dict(api_endpoint=ENDPOINT_URL)
)

index_resource_name, endpoint_resource_name = (
    find_resource_names_by_index_display_name(INDEX_DISPLAY_NAME)
)

embedding = TensorflowHubEmbeddings()
embedding = embedding.embed_documents(['Hello world.'])
embedding_documents = [
    {
        "id": str(uuid.uuid4()),
        "embedding": embedding
    }
]

result = upsert_data_points(index_resource_name, embedding_documents)